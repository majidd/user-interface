
class Config:
    APP_NAME = 'app'
    SECRET_KEY = 'this is a strong SECRET_KEY'
    LIMIT_CONFIG = {
        # These configs are for setting the request limitations for each plans.
        'CUSTOM_LIMIT_1' : '2 per minute',
        'CUSTOM_LIMIT_2' : '3 per minute',
        'CUSTOM_LIMIT_3' : '4 per minute',
        'CUSTOM_LIMIT_4' : '5 per minute',
        # If a requst hasn't a valid plan number, sets a basic plan for it.
        'CUSTOM_LIMIT_basic' : '1 per minute'
    }

class DevelopmentConfig(Config):
    DEBUG = True
    URL_CONFIG = {
        'ENNETAGGER' : 'http://0.0.0.0:4400/en-ne-tagger',
        'FANETAGGER' : 'http://0.0.0.0:4300/fa-ne-tagger',
        'ar-ne-tagger' : 'http://0.0.0.0:4200/ar-ne-tagger'
    }


class TestConfig(Config):
    DEBUG = True
    TESTING = True



class ProductionConfig(Config):
    DEBUG = False
    TESTING = True
    URL_CONFIG = {
        'HTMLCLEAN' : 'http://gtms:80/html_clean',
        'DELACCENT' : 'http://gtms:80/del_accent',
        'FATOKENIZER' : 'http://fa_tokenizer:80/tokenize',
        'ENTOKENIZER' : 'http://gtms:80/tokenizer/en',
        'ARTOKENIZER' : 'http://gtms:80/tokenizer/ar',
        'FANETAGGER' : 'http://10.109.191.195:4400/ner-tag',
        'ENNETAGGER' : 'http://10.107.98.132:4300/ner-tag',
        'ARNETAGGER' : 'http://10.99.21.174:4200/ner-tag',
        'FARMSTOPWORD' : 'http://gtms:80/rm_stopword/fa',
        'ENRMSTOPWORD' : 'http://gtms:80/rm_stopword/en',
        'ARRMSTOPWORD' : 'http://gtms:80/rm_stopword/ar',
        'FASTEMMER' : 'http://fa_stemmer:80/stem',
        'ENSTEMMER' : 'http://gtms:80/stemmer/en',
        'ARSTEMMER' : 'http://gtms:80/stemmer/ar'
    }
