#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append(".")
import configg
import os
from flask import Flask, abort, request, jsonify, g, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
import requests
app = Flask(__name__)

limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["200 per day", "50 per hour"]
)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

# extensions
db = SQLAlchemy(app)
auth = HTTPBasicAuth()

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    plan = db.Column(db.Integer)
    services = db.Column(db.String(64))
    count_total_req = db.Column(db.Integer)
    password_hash = db.Column(db.String(64))

    def hash_password(self, password):
        """Hash all provided password using passlib package
            and save it in password_hash attribute of User"""
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        """verify entered password by verify method of
            passlib.app.custom_app_context"""
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=7776000):
        """Generate a random text as token using the
            TimedJSONWebSignatureSerializer form itsdangerous package"""
        s = Serializer(config.SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        """verify the provided token if it valid
            and then returns the matching user"""
        s = Serializer(config.SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

@auth.verify_password
def verify_password(username_or_token, password):
    #first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        #try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


@app.route('/users', methods=['POST'])
def new_user():
    username = request.json.get('username')
    plan = request.json.get('plan')
    count_total_req = 0
    services = request.json.get('services')
    password = request.json.get('password')

    if username is None or plan is None or password is None:
        abort(400)    # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        abort(400)    # existing user , write the error message later on
    user = User(username=username, plan=plan, count_total_req=count_total_req, services=services)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return (jsonify({'username': user.username, 'plan': user.plan, 'services': user.services }), 201,
            {'Location': url_for('get_user', id=user.id, _external=True)})


@app.route('/users/<int:id>')
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return jsonify({'username': user.username})


@app.route('/token')
@auth.login_required
def get_auth_token():
    """generate token for registerd users, valid for 3 months"""
    token = g.user.generate_auth_token(7776000)
    return jsonify({'token': token.decode('ascii'), 'duration': 7776000})

def custom_rate_limit():
    """Implement a custom rate limit in order to apply to the endpoints"""
    if g.user:
        print("Username = ",g.user.username," Selected plan = " ,g.user.plan)
    if g.user.plan == 1:
        return config.LIMIT_CONFIG['CUSTOM_LIMIT_1']
    elif g.user.plan == 2:
        return config.LIMIT_CONFIG['CUSTOM_LIMIT_2']
    elif g.user.plan == 3:
        return config.LIMIT_CONFIG['CUSTOM_LIMIT_3']
    elif g.user.plan == 4:
        return config.LIMIT_CONFIG['CUSTOM_LIMIT_4']
    else:
        return config.LIMIT_CONFIG['CUSTOM_LIMIT_basic']



@app.route('/<variable>', methods=['POST'])
@auth.login_required
@limiter.limit(custom_rate_limit)
def post_to_requested_service(variable):
    print("total requests = ",g.user.count_total_req)
    if variable in g.user.services:
        # The input of POST can be in text form instead of Json. Then decode to utf-8
        b_data = request.data
       # text = b_data.decode("utf-8")
        if len(b_data) < 100:
            #Check the length of the input text to be shorter than specific value
            if g.user.count_total_req < 18:
                #Check the total request of user to be under the maximum value
                response = requests.request("POST", config.URL_CONFIG[variable], data=b_data)
                g.user.count_total_req = g.user.count_total_req+1
                return response.text
            else:
                return "You exceed to your maximum total request limitaion!"
        else:
            return "the input text is bigger than 100 tokens!"
    else:
        return "Unauthorized Access to this service!"



if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        db.create_all()
    # Load configurations:
    # check the provided argument for selecting the env for importing the proper
    # config class to use
    env = sys.argv[1] if len(sys.argv) >= 2 else 'dev'
    if env == 'dev':
        config = configg.DevelopmentConfig
    elif env == 'test':
        config = configg.TestConfig
    elif env == 'prod':
        config = configg.ProductionConfig
    else:
        raise ValueError('Invalid environment name')
    print(f"{env} config loaded")
    app.run(host='0.0.0.0',port=2000, use_reloader=True)
