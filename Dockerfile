FROM python:3.6
RUN pip install flask flask_sqlalchemy flask_limiter flask_httpauth itsdangerous passlib requests

COPY ./src /src
WORKDIR /.
CMD ["python", "src/run.py", "prod"]
