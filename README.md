# User Interface API-doc

## version 0.1

## Introduction

This service accepts all incoming requests, and check authentication/authorization then forward to the  pre-processing services and send back the answer to the users.

# Specifications

### Endpoints
| endpoint             | method               |
| -------------------- | -------------------- |
| /users           | POST                 |
| /tokens           | GET                 |
| /`service_enpoints`           | POST        |

## Rsgistering the users
The endpoint for registering the user is:
```
POST -> /users
```
The only accepted method is `POST`.

### Input Fields
Input fields which must provided:

`username` : username must be unique

`password` : currently with no constrains

`plan` : plan is a number between 1 to 4 which any of them has specifications for request limitation. If plan don't provided the request limit falls to basic request number.

`services` : By this field we can determine that what services can the user access.

### Response codes

`201 CREATED` : The user by the provided username and password is created successfully.

`400 BAD REQUEST` : The server can't understand the request. May be some fields are empty or username is taken by others.

`405 METHOD NOT ALLOWED` : A request was made of a resource using a request method not support by that resource.

`404 NOT FOUND` : The requested resource could not be found. Check the URL and try again.

`401 UNAUTHORIZED ACCESS` : This user can't access to the requested resource.

## Get the token

The endpoint for generate the token is:
```
GET -> /tokens
```

After a user registered, by a http request to the `/tokens`, user can get a token for authentication future requests to the services.

### Input Fields

`username`

`password`

## Access to the services

The endpoint for using the services is:
```
POST -> /SERVICENAME
```
This endpoint is dynamic which means, we can access to all services using only by this endpoint. The `SERVICENAME` is replaced with the service which we need it. For example if we need farsi-ne-tagger, just replace the `SERVICENAME` with `FANETAGGER`.

### Response codes

`200 OK` : The response of requested service is returned successfully.

`405 METHOD NOT ALLOWED` : A request was made of a resource using a request method not support by that resource.

`404 NOT FOUND` : The requested resource could not be found. Check the URL and try again.

`401 UNAUTHORIZED ACCESS` : This user can't access to the requested resource.

`500 INTERNAL SERVER ERROR` : The server is not responding!

# Authentication

The `basic_auth` is used for authenticate the users. All endpoints is `login_required` which means only registered users can access the endpoints.
